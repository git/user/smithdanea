# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

inherit eutils java-pkg-opt-2 linux-info

DESCRIPTION="Blocks connections from/to hosts listed in files using iptables."
HOMEPAGE="http://iplist.sourceforge.net/"
SRC_URI="mirror://sourceforge/${PN}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="java"

DEPEND="dev-libs/libpcre
	net-firewall/iptables
	net-libs/libnetfilter_queue
	net-libs/libnfnetlink
	sys-libs/zlib"
RDEPEND="java? ( >=virtual/jre-1.5 
	dev-util/netbeans )
	${DEPEND}"
CONFIG_CHECK="NETFILTER_XT_MATCH_IPRANGE"

src_prepare() {
	epatch ${FILESDIR}/${P}-Makefile.diff
}

src_install() {
	doinitd gentoo/ipblock
	#if use java ; then
#		insinto "/usr/share/${PN}"
#		doins ipblockUI.jar || die
#	fi
	dosbin iplist || die
	dosbin ipblock || die
	insinto /etc/cron.daily
	newins debian/ipblock.cron.daily ipblock || die
	dodoc THANKS changelog || die
	doman {${PN},ipblock}.8 || die
}

pkg_postinst() {
	einfo "A cron file was put in /etc/cron.daily."
	einfo "If you want it to update your lists daily"
	einfo "chmod u+x /etc/cron.daily/ipblock.cron.daily."
}
